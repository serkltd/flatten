## Flatten Challenge

An algorithm to flatten arbitrarily nested values, achieved by using recursion.

## Testing

Run `go test` to execute various test cases.