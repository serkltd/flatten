package flatten

import (
	"reflect"
	"testing"
)

// Define a model for test cases
type testCase struct {
	values			[]interface{}
	expected		[]interface{}
	test			string
}

// Define test cases
var (
	testCases = []testCase {
		{
			values:			[]interface{}{1, []interface{}{2, []interface{}{3}}, 4},
			expected:		[]interface{}{1, 2, 3, 4},
			test:			"Integers (challenge example)",
		},
		{
			values:			[]interface{}{34, []interface{}{[]interface{}{74, 23}, 0, []interface{}{[]interface{}{3, []interface{}{nil, nil}}}, []interface{}{[]interface{}{interface{}(nil)}}}, 88},
			expected:		[]interface{}{34, 74, 23, 0, 3, nil, nil, nil, 88},
			test:			"Integers and nil values",
		},
		{
			values:			[]interface{}{5, []interface{}{-222, []interface{}{0, -5}}, 6, -11},
			expected:		[]interface{}{5, -222, 0, -5,  6, -11},
			test:			"Integers and negative values",
		},
		{
			values:			[]interface{}{"a", "b", []interface{}{[]interface{}{"c"}, "d", []interface{}{"e", []interface{}{[]interface{}{"f", []interface{}{"g", "h"}}}}, "i", "j"}},
			expected:		[]interface{}{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"},
			test:			"Strings",
		},
		{
			values:			[]interface{}{"x", 5, []interface{}{-222, []interface{}{[]interface{}{"y", []interface{}{"z"}}, 0, -5}}, 6, -11},
			expected:		[]interface{}{"x", 5, -222, "y", "z", 0, -5, 6, -11},
			test:			"Mixed integers and strings",
		},
		{
			values:			[]interface{}{10, -22, 0, 3, -3, 99},
			expected:		[]interface{}{10, -22, 0, 3, -3, 99},
			test:			"Flattened Input",
		},
	}
)

func TestFlatten(t *testing.T) {

	// Range over test cases
	for _, testCase := range testCases {

		// flatten test values
		result := flatten(testCase.values)

		// Recursively compare concrete values in result to expected result
		if  !reflect.DeepEqual(result, testCase.expected) {
			t.Fatalf(":( Test Fail - %s\nResult: %v\nExpected result: %v", testCase.test, result, testCase.expected)
		}
		t.Logf(":) Test Pass - %s", testCase.test)
	}
}