package flatten

func flatten(values []interface{}) (arr []interface{}) {

	// Range over values
	for _, value := range values {

		// Assert type is interface{} - if true, recursively append the flattened result to array
		// Else simply append value to array
		if i, ok := value.([]interface{}); ok {
			arr = append(arr, flatten(i)...)
		} else {
			arr = append(arr, value)
		}

	}
	return
}